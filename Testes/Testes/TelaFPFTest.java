package Testes;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.JSpinner;

import org.junit.jupiter.api.Test;

class TelaFPFTest {

	@Test
	void testJSpinner() {
		//fail("Not yet implemented");
		JSpinner tensaoSpinner = new JSpinner();
		tensaoSpinner.setBounds(420, 110, 110, 20);
		tensaoSpinner.setValue(35);
		
		assertEquals((double)(Integer)tensaoSpinner.getValue(), 35.00d);
		
	}
	
	@Test
	void testPotenciaAtiva() {
		double tensaoAF = 0;
		double correnteAF = 35;
		double tensaoAmplitude = 220;
		double correnteAmplitude = 39;
		double potenciaAtivaEsperada = 7028.0d;
		double potenciaAtiva = ( (Math.cos( Math.toRadians(tensaoAF - correnteAF) )) * tensaoAmplitude * correnteAmplitude);
		
		assertEquals(potenciaAtiva, potenciaAtivaEsperada, 0.0d);
		
	}

}
