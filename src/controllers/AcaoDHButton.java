package controllers;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.TelaDH;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class AcaoDHButton implements ActionListener{
	
	private JPanel painelInicial;
	private JFrame telaInicial;


	public AcaoDHButton(JPanel painelInicial, JFrame telaInicial) {
		this.painelInicial=painelInicial;
		this.telaInicial=telaInicial;
	}
	
public void actionPerformed(ActionEvent e){
			
		
		painelInicial.setVisible(false);
		try {
			new TelaDH(telaInicial);
		} catch (Exception e1) {
			System.out.println("Erro desconhecido!!");
		}
	}
	
}
