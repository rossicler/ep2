package controllers;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import view.TelaFPF;

public class AcaoFPFButton implements ActionListener{
	
	private JPanel painelInicial;
	private JFrame telaInicial;


	public AcaoFPFButton(JPanel painelInicial, JFrame telaInicial) {
		this.painelInicial=painelInicial;
		this.telaInicial=telaInicial;
	}
	
public void actionPerformed(ActionEvent e){
			
		
		painelInicial.setVisible(false);
		try {
			new TelaFPF(telaInicial);
		} catch (Exception e1) {
			System.out.println("Erro desconhecido!!");
		}
	}
	
}
