package controllers;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.TelaFPH;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class AcaoFPHButton implements ActionListener{
	
	private JPanel painelInicial;
	private JFrame telaInicial;


	public AcaoFPHButton(JPanel painelInicial, JFrame telaInicial) {
		this.painelInicial=painelInicial;
		this.telaInicial=telaInicial;
	}
	
public void actionPerformed(ActionEvent e){
			
		
		painelInicial.setVisible(false);
		try {
			new TelaFPH(telaInicial);
		} catch (Exception e1) {
			System.out.println("Erro desconhecido!!");
		}
	}
	
}
