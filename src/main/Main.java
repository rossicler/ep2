package main;

import java.awt.EventQueue;

import javax.swing.JFrame;

import view.TelaPrincipal;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame telaInicial = new JFrame();
					telaInicial.setBounds(100, 100, 750, 600);
					telaInicial.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					telaInicial.setResizable(false);
					new TelaPrincipal(telaInicial);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
