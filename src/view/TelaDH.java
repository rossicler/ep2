package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import grafico.GraphPanel;
import interfaces.Tela;

public class TelaDH extends JFrame implements Tela {
	
	private int minValueX = 0;
	private int maxValueX = 50;
	private int step = 1;
	private JPanel contentPane;
	private JPanel componenteFundamentalPane;
	private JPanel harmonico1Pane;
	private JPanel harmonico2Pane;
	private JPanel resultadoPane;
	private JSpinner componenteFundamentalSpinner = new JSpinner();
	private JSpinner componenteAmplitudeSpinner = new JSpinner();
	private JSpinner harmonico1Spinner = new JSpinner();
	private JSpinner harmonico1AmplitudeSpinner = new JSpinner();
	private JSpinner harmonico2Spinner = new JSpinner();
	private JSpinner harmonico2AmplitudeSpinner = new JSpinner();
	private JSpinner numeroHarmonicasSpinner = new JSpinner();
	private JSpinner ordemH1Spinner = new JSpinner();
	private JSpinner ordemH2Spinner = new JSpinner();
	private int componenteFundamental = 0;
	private int harmonico1 = 30;
	private int harmonico2 = -90;
	private int componenteAmplitude = 220;
	private int harmonico1Amplitude = 20;
	private int harmonico2Amplitude = 15;
	private int ordemH1 = 3;
	private int ordemH2 = 5;
	private int numeroHarmonicas = 2;
	JFormattedTextField serieFourierResult;
	
	
	public TelaDH(JFrame telaInicial){
		DesenhaTela(telaInicial, contentPane);
	}	
	
	public void DesenhaTela(JFrame telaInicial, JPanel painelInicial) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEntradas = new JLabel("Entradas");
		lblEntradas.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEntradas.setHorizontalAlignment(SwingConstants.CENTER);
		lblEntradas.setBounds(325, 11, 100, 25);
		contentPane.add(lblEntradas);
		
		JLabel lblSaidas = new JLabel("Saidas");
		lblSaidas.setHorizontalAlignment(SwingConstants.CENTER);
		lblSaidas.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSaidas.setBounds(325, 450, 100, 25);
		contentPane.add(lblSaidas);
		
		//---------------------------------------------------------------
		componenteFundamentalPane = new JPanel(new GridLayout(1,1));
		componenteFundamentalPane.setBounds(20, 70, 350,80);
		
		JLabel lblCF = new JLabel("Componente Fundamental");
		lblCF.setBounds(115, 50, 150, 20);
		lblCF.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblCF);
		
		JLabel lblCF_AF = new JLabel("Angulo de fase [�C]");
		lblCF_AF.setBounds(380, 70, 110, 20);
		lblCF_AF.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblCF_AF);
		
		SpinnerNumberModel componenteFundamentalModel = new SpinnerNumberModel();
		componenteFundamentalModel.setMaximum(180);
		componenteFundamentalModel.setMinimum(-180);
		componenteFundamentalSpinner.setModel(componenteFundamentalModel);
		componenteFundamentalSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				componenteFundamental = (Integer) componenteFundamentalSpinner.getValue();
			}
		});
		componenteFundamentalSpinner.setBounds(380, 90, 110, 20);
		contentPane.add(componenteFundamentalSpinner);
		
		JLabel lblCF_A = new JLabel("Amplitude");
		lblCF_A.setBounds(380, 110, 110, 20);
		lblCF_A.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblCF_A);
		
		SpinnerNumberModel componenteAmplitudeSpinnerModel = new SpinnerNumberModel();
		componenteAmplitudeSpinnerModel.setMaximum(220);
		componenteAmplitudeSpinnerModel.setMinimum(0);
		componenteAmplitudeSpinner.setModel(componenteAmplitudeSpinnerModel);
		componenteAmplitudeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				componenteAmplitude = (Integer) componenteAmplitudeSpinner.getValue();
			}
		});
		componenteAmplitudeSpinner.setBounds(380, 130, 110, 20);
		contentPane.add(componenteAmplitudeSpinner);
		
		componenteFundamentalPane.add(DesenhaGraficoFundamental());
		
		//----------------------------------------------------------------
		harmonico1Pane = new JPanel(new GridLayout(1,1));
		harmonico1Pane.setBounds(20,220,350,80);
		
		JLabel lblH1_AF = new JLabel("Angulo de fase [�C]");
		lblH1_AF.setBounds(380, 220, 110, 20);
		lblH1_AF.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblH1_AF);
		

		SpinnerNumberModel harmonico1SpinnerModel = new SpinnerNumberModel();
		harmonico1SpinnerModel.setMaximum(180);
		harmonico1SpinnerModel.setMinimum(-180);
		harmonico1Spinner.setModel(harmonico1SpinnerModel);
		harmonico1Spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				harmonico1 = (Integer) harmonico1Spinner.getValue();
			}
		});
		harmonico1Spinner.setBounds(380, 240, 110, 20);
		contentPane.add(harmonico1Spinner);
		
		JLabel lblH1_A = new JLabel("Amplitude");
		lblH1_A.setBounds(380, 260, 110, 20);
		lblH1_A.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblH1_A);
		

		SpinnerNumberModel harmonico1AmplitudeSpinnerModel = new SpinnerNumberModel();
		harmonico1AmplitudeSpinnerModel.setMaximum(100);
		harmonico1AmplitudeSpinnerModel.setMinimum(0);
		harmonico1AmplitudeSpinner.setModel(harmonico1AmplitudeSpinnerModel);
		harmonico1AmplitudeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				harmonico1Amplitude = (Integer) harmonico1AmplitudeSpinner.getValue();
			}
		});
		harmonico1AmplitudeSpinner.setBounds(380, 280, 110, 20);
		contentPane.add(harmonico1AmplitudeSpinner);
		
		JLabel lblOrdemH1 = new JLabel("Ordem Harmonica");
		lblOrdemH1.setBounds(560, 220, 110, 20);
		lblOrdemH1.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblOrdemH1);
		
		SpinnerNumberModel ordemH1SpinnerModel = new SpinnerNumberModel();
		ordemH1SpinnerModel.setMaximum(15);
		ordemH1SpinnerModel.setMinimum(0);
		ordemH1Spinner.setModel(ordemH1SpinnerModel);
		ordemH1Spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				ordemH1 = (Integer) ordemH1Spinner.getValue();
			}
		});
		ordemH1Spinner.setBounds(560, 240, 110, 20);
		contentPane.add(ordemH1Spinner);
		
		harmonico2Pane = new JPanel(new GridLayout(1,1));
		harmonico2Pane.setBounds(20,320,350,80);
		
		JLabel lblH2_AF = new JLabel("Angulo de fase [�C]");
		lblH2_AF.setBounds(380, 320, 110, 20);
		lblH2_AF.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblH2_AF);
		
		SpinnerNumberModel harmonico2SpinnerModel = new SpinnerNumberModel();
		harmonico2SpinnerModel.setMaximum(180);
		harmonico2SpinnerModel.setMinimum(-180);
		harmonico2Spinner.setModel(harmonico2SpinnerModel);
		harmonico2Spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				harmonico2 = (Integer) harmonico2Spinner.getValue();
			}
		});
		harmonico2Spinner.setBounds(380, 340, 110, 20);
		contentPane.add(harmonico2Spinner);
		
		JLabel lblH2_A = new JLabel("Amplitude");
		lblH2_A.setBounds(380, 360, 110, 20);
		lblH2_A.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblH2_A);
		

		SpinnerNumberModel harmonico2AmplitudeSpinnerModel = new SpinnerNumberModel();
		harmonico2AmplitudeSpinnerModel.setMaximum(100);
		harmonico2AmplitudeSpinnerModel.setMinimum(0);
		harmonico2AmplitudeSpinner.setModel(harmonico2AmplitudeSpinnerModel);
		harmonico2AmplitudeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				harmonico2Amplitude = (Integer) harmonico2AmplitudeSpinner.getValue();
			}
		});
		harmonico2AmplitudeSpinner.setBounds(380, 380, 110, 20);
		contentPane.add(harmonico2AmplitudeSpinner);
		
		JLabel lblOrdemH2 = new JLabel("Ordem Harmonica");
		lblOrdemH2.setBounds(560, 320, 110, 20);
		lblOrdemH2.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblOrdemH2);
		
		SpinnerNumberModel ordemH2SpinnerModel = new SpinnerNumberModel();
		ordemH2SpinnerModel.setMaximum(15);
		ordemH2SpinnerModel.setMinimum(0);
		ordemH2Spinner.setModel(ordemH2SpinnerModel);
		ordemH2Spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				ordemH2 = (Integer) ordemH2Spinner.getValue();
			}
		});
		ordemH2Spinner.setBounds(560, 340, 110, 20);
		contentPane.add(ordemH2Spinner);
		
		JLabel lblNH = new JLabel("Numero de ordens harmonicas");
		lblNH.setBounds(380, 160, 180, 20);
		lblNH.setHorizontalAlignment(SwingConstants.LEADING);
		contentPane.add(lblNH);
		
		SpinnerNumberModel numeroHarmonicasSpinnerModel = new SpinnerNumberModel();
		numeroHarmonicasSpinnerModel.setMaximum(6);
		numeroHarmonicasSpinnerModel.setMinimum(0);
		numeroHarmonicasSpinner.setModel(numeroHarmonicasSpinnerModel);
		numeroHarmonicasSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				numeroHarmonicas = (Integer) numeroHarmonicasSpinner.getValue();
			}
		});
		numeroHarmonicasSpinner.setBounds(380, 180, 110, 20);
		contentPane.add(numeroHarmonicasSpinner);
		
		JLabel lblH1 = new JLabel("Harmonicas");
		lblH1.setBounds(115, 180, 150, 20);
		lblH1.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblH1);
		
		harmonico1Pane.add(DesenhaGraficoHarmonicas(harmonico1, ordemH1, harmonico1Amplitude));
		harmonico2Pane.add(DesenhaGraficoHarmonicas(harmonico2, ordemH2, harmonico2Amplitude));
		
		//-------------------------------------------------------------------------------
		resultadoPane = new JPanel(new GridLayout(1,1));
		resultadoPane.setBounds(20, 480, 350, 80);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setBounds(135, 460, 100, 20);
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblResultado);
		
		JLabel lblSFAF = new JLabel("Serie de Fourier Amplitude-Fase");
		lblSFAF.setBounds(460, 500, 200, 20);
		lblSFAF.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblSFAF);
		
		serieFourierResult = new JFormattedTextField("f(t)="+ componenteAmplitude + "cos(wt)" + harmonico1Amplitude + "cos(" + ordemH1 + "wt+" + harmonico1 +"�)+" + harmonico2Amplitude + "cos(" + ordemH2 + "wt-" + harmonico2 + "�)" );
		serieFourierResult.setHorizontalAlignment(SwingConstants.LEADING);
		serieFourierResult.setBounds(420, 520, 280, 20);
		serieFourierResult.setEditable(false);
		contentPane.add(serieFourierResult);
	
		resultadoPane.add(DesenhaGraficoResultante(numeroHarmonicas));
		
		//-------------------------------------------------------------------------------
		JButton OKButton = new JButton("OK");
        OKButton.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		componenteFundamentalPane.removeAll();
        		componenteFundamentalPane.add(DesenhaGraficoFundamental());
        		componenteFundamentalPane.revalidate();
        		componenteFundamentalPane.repaint();
        		
        		harmonico1Pane.removeAll();
        		harmonico1Pane.add(DesenhaGraficoHarmonicas(harmonico1, ordemH1, harmonico1Amplitude));
        		harmonico1Pane.revalidate();
        		harmonico1Pane.repaint();
        		
        		harmonico2Pane.removeAll();
        		harmonico2Pane.add(DesenhaGraficoHarmonicas(harmonico2, ordemH2, harmonico2Amplitude));
        		harmonico2Pane.revalidate();
        		harmonico2Pane.repaint();
        		
        		resultadoPane.removeAll();
        		resultadoPane.add(DesenhaGraficoResultante(numeroHarmonicas));
        		resultadoPane.revalidate();
        		resultadoPane.repaint();
        		
        		serieFourierResult.setText("f(t)="+ componenteAmplitude + "cos(wt)" + harmonico1Amplitude + "cos(" + ordemH1 + "wt+" + harmonico1 +"�)+" + harmonico2Amplitude + "cos(" + ordemH2 + "wt-" + harmonico2 + "�)" );
        		
        	}
        });
        OKButton.setBackground(Color.WHITE);
        OKButton.setBounds(650, 380, 60, 20);
        OKButton.setActionCommand("Tensao");
		contentPane.add(OKButton);
		//-------------------------------------------------------------------------------
		contentPane.add(componenteFundamentalPane);
		contentPane.add(harmonico1Pane);
		contentPane.add(harmonico2Pane);
		contentPane.add(resultadoPane);
		telaInicial.add(contentPane);
		contentPane.setVisible(true);
		telaInicial.setVisible(true);
	}
	
	private GraphPanel DesenhaGraficoFundamental(){
		List<Double> scoresFundamental = new ArrayList<>();
		
		for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloFundamental = (double) componenteFundamental + (double) (2*Math.PI*60* i);
        	scoresFundamental.add((double)(Math.cos(Math.toRadians(anguloFundamental)) *  componenteAmplitude));
        }
		
		GraphPanel grafico = new GraphPanel(scoresFundamental);
		
		return grafico;
	}

	private GraphPanel DesenhaGraficoHarmonicas(int harmonico, int ordemH, int harmonicoAmplitude) {
		List<Double>scoresHarmonicos = new ArrayList<>();
		
		for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloHarmonicos = (double) harmonico + (double) (2*Math.PI*60* i * ordemH);
        	scoresHarmonicos.add((double)(Math.cos(Math.toRadians(anguloHarmonicos)) *  harmonicoAmplitude));
        }
		
		GraphPanel grafico = new GraphPanel(scoresHarmonicos);
		
		return grafico;
	}
	
	private GraphPanel DesenhaGraficoResultante(int numeroHarmonicas) {
		List<Double> scoresResultado = new ArrayList<>();
		for (int i = minValueX; i < maxValueX; i = i + step) {
	        double anguloFundamental = (double) componenteFundamental + (double) (2*Math.PI*60* i);
	        double anguloHarmonicos1 = (double) harmonico1 + (double) (2*Math.PI*60* i * ordemH1);
	        double anguloHarmonicos2 = (double) harmonico2 + (double) (2*Math.PI*60* i * ordemH2);
	        scoresResultado.add((double)(Math.cos(Math.toRadians(anguloFundamental)) *  componenteAmplitude) + (double)(Math.cos(Math.toRadians(anguloHarmonicos1)) *  harmonico1Amplitude) + (double)(Math.cos(Math.toRadians(anguloHarmonicos2)) *  harmonico2Amplitude));
	    }
			
		GraphPanel grafico = new GraphPanel(scoresResultado);
		
		return grafico;
	}
	
	
}
