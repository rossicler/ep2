package view;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import grafico.GraphPanel;
import interfaces.Tela;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class TelaFPF extends JFrame implements Tela {
	
	private JPanel painelInicial;
	private int minValueX = 0;
	private int maxValueX = 50;
	private int step = 1;
	private DecimalFormat df;
	private JPanel tensaoPanel;
	private JPanel correntePanel;
	private JPanel PIPanel;
	private JSpinner tensaoSpinner = new JSpinner();
	private int tensaoAF = 0;
	private JSpinner tensaoAmplitudeSpinner = new JSpinner();
	private int tensaoAmplitude = 220;
	private JSpinner correnteSpinner = new JSpinner();
	private int correnteAF = 35;
	private JSpinner correnteAmplitudeSpinner = new JSpinner();
	private int correnteAmplitude = 39;
	private JButton OKButton;
	JFormattedTextField potenciaAtivaResult;
	JFormattedTextField potenciaReativaResult;
	JFormattedTextField potenciaAparenteResult;
	JFormattedTextField fatorPotenciaResult;
	
	public TelaFPF(JFrame telaInicial) {
		DesenhaTela(telaInicial, painelInicial);
		
	}
	
	public void DesenhaTela(JFrame telaInicial, JPanel painelInicial){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 600);
		painelInicial = new JPanel();
		painelInicial.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(painelInicial);
		painelInicial.setLayout(null);
		
		df = new DecimalFormat("###,##0.00");
		
		tensaoPanel = new JPanel(new GridLayout(1,1));
		tensaoPanel.setBackground(Color.LIGHT_GRAY);
		tensaoPanel.setBounds(20, 70, 350, 80);
		
		JLabel tensaoLabel = new JLabel("Tensao");
		tensaoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		tensaoLabel.setBounds(145, 50, 80, 15);
		painelInicial.add(tensaoLabel);
		
		JLabel TAFLabel = new JLabel("Angulo de fase [�C]");
		TAFLabel.setBounds(420,70,110,20);
		painelInicial.add(TAFLabel);
		
		JLabel TALabel = new JLabel("Amplitude");
		TALabel.setBounds(420,110,110,20);
		painelInicial.add(TALabel);
		
		SpinnerNumberModel tensaoAmpModel = new SpinnerNumberModel();
		tensaoAmpModel.setMaximum(220);
		tensaoAmpModel.setMinimum(0);
		tensaoAmplitudeSpinner.setModel(tensaoAmpModel);
		tensaoAmplitudeSpinner.setBounds(420, 130, 110, 20);
		tensaoAmplitudeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				tensaoAmplitude = (Integer) tensaoAmplitudeSpinner.getValue();
			}
		});
		painelInicial.add(tensaoAmplitudeSpinner);
		
		tensaoSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				tensaoAF = (Integer) tensaoSpinner.getValue();
			}
		});
		
		SpinnerNumberModel tensaoModel = new SpinnerNumberModel();
		tensaoModel.setMaximum(180);
		tensaoModel.setMinimum(-180);
		tensaoSpinner.setModel(tensaoModel);
		tensaoSpinner.setBounds(420, 90, 110, 20);
		painelInicial.add(tensaoSpinner);
		
		tensaoPanel.add(DesenhaGraficoTensao());
		painelInicial.add(tensaoPanel);
		
		//---------------------------------------------------------------------------------------------
		correntePanel = new JPanel(new GridLayout(1,1));
		correntePanel.setBackground(Color.LIGHT_GRAY);
		correntePanel.setBounds(20, 200, 350, 80);
		
		JLabel lblCorrente = new JLabel("Corrente");
		lblCorrente.setHorizontalAlignment(SwingConstants.CENTER);
		lblCorrente.setBounds(145, 180, 80, 15);
		painelInicial.add(lblCorrente);
		
		JLabel CAFLabel = new JLabel("Angulo de fase [�C]");
		CAFLabel.setBounds(420, 200, 110, 20);
		painelInicial.add(CAFLabel);
		
		SpinnerNumberModel correnteModel = new SpinnerNumberModel();
		correnteModel.setMaximum(180);
		correnteModel.setMinimum(-180);
		correnteSpinner.setModel(correnteModel);
		correnteSpinner.setBounds(420, 220, 110, 20);
		correnteSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				correnteAF = (Integer) correnteSpinner.getValue();
			}
		});
		painelInicial.add(correnteSpinner);
		
		JLabel CALabel = new JLabel("Amplitude");
		CALabel.setBounds(420, 240, 110, 20);
		painelInicial.add(CALabel);
		
		SpinnerNumberModel correnteAmpModel = new SpinnerNumberModel();
		correnteAmpModel.setMaximum(180);
		correnteAmpModel.setMinimum(-180);
		correnteAmplitudeSpinner.setModel(correnteAmpModel);
		correnteAmplitudeSpinner.setBounds(420, 260, 110, 20);
		correnteAmplitudeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				correnteAmplitude = (Integer) correnteAmplitudeSpinner.getValue();
			}
		});
		painelInicial.add(correnteAmplitudeSpinner);
        
        correntePanel.add(DesenhaGraficoCorrente());
        painelInicial.add(correntePanel);
		
		//---------------------------------------------------------------------------------------------
		PIPanel = new JPanel(new GridLayout(1,1));
		PIPanel.setBackground(Color.LIGHT_GRAY);
		PIPanel.setBounds(20, 360, 350, 80);
        
        PIPanel.add(DesenhaGraficoResultante());
        painelInicial.add(PIPanel);
		
		JLabel lblPotnciaInstantnea = new JLabel("Potencia Instantanea");
		lblPotnciaInstantnea.setHorizontalAlignment(SwingConstants.CENTER);
		lblPotnciaInstantnea.setBounds(90, 340, 200, 15);
		painelInicial.add(lblPotnciaInstantnea);
		
		JLabel lblEntradas = new JLabel("Entradas");
		lblEntradas.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEntradas.setHorizontalAlignment(SwingConstants.CENTER);
		lblEntradas.setBounds(325, 11, 100, 25);
		painelInicial.add(lblEntradas);
		
		JLabel lblSadas = new JLabel("Saidas");
		lblSadas.setHorizontalAlignment(SwingConstants.CENTER);
		lblSadas.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSadas.setBounds(325, 305, 100, 25);
		painelInicial.add(lblSadas);
		//--------------------------------------------------------------------------
		JLabel lblPotenciaAtiva = new JLabel("Potencia Ativa:");
		lblPotenciaAtiva.setBounds(20, 460, 150, 20);
		painelInicial.add(lblPotenciaAtiva);
		
		float potenciaAtiva = ( (float) Math.cos( Math.toRadians((double)(tensaoAF - correnteAF)) ) * tensaoAmplitude * correnteAmplitude);
		potenciaAtivaResult = new JFormattedTextField(df.format(potenciaAtiva) + " Watts");
		potenciaAtivaResult.setHorizontalAlignment(SwingConstants.LEADING);
		potenciaAtivaResult.setBounds(170, 460, 200, 20);
		potenciaAtivaResult.setEditable(false);
		painelInicial.add(potenciaAtivaResult);
		//--------------------------------------------------------------------------
		JLabel lblPotenciaReativa = new JLabel("Potencia Reativa:");
		lblPotenciaReativa.setBounds(20, 485, 150, 20);
		painelInicial.add(lblPotenciaReativa);
		
		float potenciaReativa = ( (float) Math.sin( Math.toRadians((double) (tensaoAF - correnteAF)) ) * tensaoAmplitude * correnteAmplitude);
		potenciaReativaResult = new JFormattedTextField(df.format(potenciaReativa) + " VAR");
		potenciaReativaResult.setHorizontalAlignment(SwingConstants.LEADING);
		potenciaReativaResult.setBounds(170, 485, 200, 20);
		potenciaReativaResult.setEditable(false);
		painelInicial.add(potenciaReativaResult);
		//--------------------------------------------------------------------------
		JLabel lblPotenciaAparente = new JLabel("Potencia Aparente:");
		lblPotenciaAparente.setBounds(20, 510, 150, 20);
		painelInicial.add(lblPotenciaAparente);
		
		float potenciaAparente = ( (float) tensaoAmplitude * correnteAmplitude);
		JFormattedTextField potenciaAparenteResult = new JFormattedTextField(df.format(potenciaAparente) + " VA");
		potenciaAparenteResult.setHorizontalAlignment(SwingConstants.LEADING);
		potenciaAparenteResult.setBounds(170, 510, 200, 20);
		potenciaAparenteResult.setEditable(false);
		painelInicial.add(potenciaAparenteResult);
		//--------------------------------------------------------------------------
		JLabel lblFatorPotencia = new JLabel("Fator de Potencia:");
		lblFatorPotencia.setBounds(20, 535, 150, 20);
		painelInicial.add(lblFatorPotencia);
		
		float fatorPotencia = (float) Math.cos( Math.toRadians(tensaoAF - correnteAF) );
		String FP;
		if (fatorPotencia<0) {
			FP = "fator de potencia adiantado";
		}
		else {
			FP = "fator de potencia atrasado";
		}
		fatorPotenciaResult = new JFormattedTextField(df.format(fatorPotencia) + " - " + FP);
		fatorPotenciaResult.setHorizontalAlignment(SwingConstants.LEADING);
		fatorPotenciaResult.setBounds(170, 535, 200, 20);
		fatorPotenciaResult.setEditable(false);
		painelInicial.add(fatorPotenciaResult);
		//--------------------------------------------------------------------------
		OKButton = new JButton("OK");
		OKButton.setBackground(Color.WHITE);
		OKButton.setBounds(650, 260, 60, 20);
		OKButton.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {	
        		tensaoPanel.removeAll();
        		tensaoPanel.add(DesenhaGraficoTensao());
        		tensaoPanel.revalidate();
        		tensaoPanel.repaint();
        		
        		correntePanel.removeAll();
        		correntePanel.add(DesenhaGraficoCorrente());
        		correntePanel.revalidate();
        		correntePanel.repaint();
        		
        		PIPanel.removeAll();
        		PIPanel.add(DesenhaGraficoResultante());
        		PIPanel.revalidate();
        		PIPanel.repaint();
        		
        		float potenciaAtiva = ( (float) Math.cos( Math.toRadians((double)(tensaoAF - correnteAF)) ) * tensaoAmplitude * correnteAmplitude);
        		potenciaAtivaResult.setText(df.format(potenciaAtiva) + " Watts");
        		
        		float potenciaReativa = ( (float) Math.sin( Math.toRadians((double) (tensaoAF - correnteAF)) ) * tensaoAmplitude * correnteAmplitude);
        		potenciaReativaResult.setText(df.format(potenciaReativa) + " VAR");
        		
        		float potenciaAparente = ( (float) tensaoAmplitude * correnteAmplitude);
        		potenciaAparenteResult.setText(df.format(potenciaAparente) + " VA");
        		
        		float fatorPotencia = (float) Math.cos( Math.toRadians(tensaoAF - correnteAF) );
        		String FP;
        		if (fatorPotencia<0) {
        			FP = "fator de potencia adiantado";
        		}
        		else {
        			FP = "fator de potencia atrasado";
        		}
        		fatorPotenciaResult.setText(df.format(fatorPotencia) + " - " + FP);
        	}
        });
		painelInicial.add(OKButton);
		//--------------------------------------------------------------------------
		
		telaInicial.add(painelInicial);
		painelInicial.setVisible(true);
		telaInicial.setVisible(true);
		
	}
	
	public GraphPanel DesenhaGraficoTensao() {
		List<Double> scores = new ArrayList<>();
		
		for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloTensao = (double) tensaoAF + (double) (2*Math.PI*60* i);
            scores.add((double)(Math.cos(Math.toRadians(anguloTensao)) *  tensaoAmplitude));
        } 
		
		GraphPanel grafico = new GraphPanel(scores);
		
		return grafico;
	}
	
	public GraphPanel DesenhaGraficoCorrente() {
		List<Double>scores = new ArrayList<>();
		
        for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloCorrente = (double)(correnteAF + (double) (2*Math.PI*60*i));
            scores.add((double)(Math.cos(Math.toRadians(anguloCorrente)) * correnteAmplitude) );
        }
        GraphPanel grafico = new GraphPanel(scores);
        
        return grafico;
	}
	
	public GraphPanel DesenhaGraficoResultante() {
		List<Double> scoresPI = new ArrayList<>();
		
        for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloTensaoResult = (double) tensaoAF + (double) (2*Math.PI*60* i);
        	double anguloCorrenteResult = (double)(correnteAF + (double) (2*Math.PI*60*i));
            scoresPI.add((double)(Math.cos(Math.toRadians((Math.cos(Math.toRadians(anguloTensaoResult)) * tensaoAmplitude ) * (Math.cos(Math.toRadians(anguloCorrenteResult))) * correnteAmplitude ))));
        }
		GraphPanel grafico = new GraphPanel(scoresPI);
        
		return grafico;
	}

	
	
}
