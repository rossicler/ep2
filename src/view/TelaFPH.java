package view;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import grafico.GraphPanel;
import interfaces.Tela;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TelaFPH extends JFrame implements Tela {

	private JPanel contentPane;
	private int minValueX = 0;
	private int maxValueX = 50;
	private int step = 1;
	private DecimalFormat df;
	private JPanel tensaoPanel;
	private JPanel correntePanel;
	private JPanel PIPanel;
	private JSpinner tensaoSpinner = new JSpinner();
	private JSpinner tensaoAmplitudeSpinner = new JSpinner();
	private JSpinner correnteSpinner = new JSpinner();
	private JSpinner correnteAmplitudeSpinner = new JSpinner();
	private JSpinner ordemHarmonicaSpinner = new JSpinner();
	private double potenciaLiquida = 0;
	private int tensaoAF = 0;
	private int tensaoAmplitude = 220;
	private int correnteAF = 35;
	private int correnteAmplitude  = 39;
	private int ordemHarmonica = 3;
	JFormattedTextField potenciaLiquidaResult;
	JFormattedTextField potenciaDistorcaoResult;
	JFormattedTextField TPFResult;
	
	public TelaFPH(JFrame telaInicial) {
		DesenhaTela(telaInicial, contentPane);
	}
	
	public void DesenhaTela(JFrame telaInicial, JPanel contentPane) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		df = new DecimalFormat("###,##0.00");
		
		
		tensaoPanel = new JPanel(new GridLayout(1,1));
		tensaoPanel.setBackground(Color.LIGHT_GRAY);
		tensaoPanel.setBounds(20, 70, 350, 80);
		
		JLabel tensaoLabel = new JLabel("Tensao");
		tensaoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		tensaoLabel.setBounds(145, 50, 80, 15);
		contentPane.add(tensaoLabel);
		
		JLabel TAFLabel = new JLabel("Angulo de fase [�C]");
		TAFLabel.setBounds(420,70,110,20);
		contentPane.add(TAFLabel);
		
		JLabel TALabel = new JLabel("Amplitude");
		TALabel.setBounds(420,110,110,20);
		contentPane.add(TALabel);
		
		JLabel OHLabel = new JLabel("Ordem Harmonica");
		OHLabel.setBounds(600,70,110,20);
		contentPane.add(OHLabel);
		
		ordemHarmonicaSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				ordemHarmonica = (Integer) ordemHarmonicaSpinner.getValue();
			}
		});
		ordemHarmonicaSpinner.setBounds(600, 90, 110, 20);
		contentPane.add(ordemHarmonicaSpinner);
		

		SpinnerNumberModel tensaoAmpModel = new SpinnerNumberModel();
		tensaoAmpModel.setMaximum(220);
		tensaoAmpModel.setMinimum(0);
		tensaoAmplitudeSpinner.setModel(tensaoAmpModel);
		tensaoAmplitudeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				tensaoAmplitude = (Integer) tensaoAmplitudeSpinner.getValue();
			}
		});
		tensaoAmplitudeSpinner.setBounds(420, 130, 110, 20);
		contentPane.add(tensaoAmplitudeSpinner);
		
		SpinnerNumberModel tensaoModel = new SpinnerNumberModel();
		tensaoModel.setMaximum(180);
		tensaoModel.setMinimum(-180);
		tensaoSpinner.setModel(tensaoModel);
		tensaoSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				tensaoAF = (Integer) tensaoSpinner.getValue();
			}
		});
		tensaoSpinner.setBounds(420, 90, 110, 20);
		contentPane.add(tensaoSpinner);
        
		tensaoPanel.add(DesenhaGraficoTensao());
		contentPane.add(tensaoPanel);
		
		//---------------------------------------------------------------------------------------------
		correntePanel = new JPanel(new GridLayout(1,1));
		correntePanel.setBackground(Color.LIGHT_GRAY);
		correntePanel.setBounds(20, 200, 350, 80);
		
		JLabel lblCorrente = new JLabel("Corrente");
		lblCorrente.setHorizontalAlignment(SwingConstants.CENTER);
		lblCorrente.setBounds(145, 180, 80, 15);
		contentPane.add(lblCorrente);
		
		JLabel CAFLabel = new JLabel("Angulo de fase [�C]");
		CAFLabel.setBounds(420, 200, 110, 20);
		contentPane.add(CAFLabel);
		
		SpinnerNumberModel correnteModel = new SpinnerNumberModel();
		correnteModel.setMaximum(180);
		correnteModel.setMinimum(-180);
		correnteSpinner.setModel(correnteModel);
		correnteSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				correnteAF = (Integer) correnteSpinner.getValue();
			}
		});
		correnteSpinner.setBounds(420, 220, 110, 20);
		contentPane.add(correnteSpinner);
		
		JLabel CALabel = new JLabel("Amplitude");
		CALabel.setBounds(420, 240, 110, 20);
		contentPane.add(CALabel);
		
		SpinnerNumberModel correnteAmpModel = new SpinnerNumberModel();
		correnteAmpModel.setMaximum(180);
		correnteAmpModel.setMinimum(-180);
		correnteAmplitudeSpinner.setModel(correnteAmpModel);
		correnteAmplitudeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				correnteAmplitude = (Integer) correnteAmplitudeSpinner.getValue();
			}
		});
		correnteAmplitudeSpinner.setBounds(420, 260, 110, 20);
		contentPane.add(correnteAmplitudeSpinner);
		
        correntePanel.add(DesenhaGraficoCorrente());
        contentPane.add(correntePanel);
        
		//---------------------------------------------------------------------------------------------
		PIPanel = new JPanel(new GridLayout(1,1));
		PIPanel.setBackground(Color.LIGHT_GRAY);
		PIPanel.setBounds(20, 360, 350, 120);
		
        for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloTensaoResult = (double) tensaoAF + (double) (2*Math.PI*60* i);
        	double anguloCorrenteResult = (double)(correnteAF + (double) (ordemHarmonica*2*Math.PI*60*i));
            potenciaLiquida += (Math.cos(Math.toRadians(((double)(Math.cos(Math.toRadians(anguloTensaoResult)) * tensaoAmplitude)) * ((double)(Math.cos(Math.toRadians(anguloCorrenteResult))) * correnteAmplitude))));
        }
        
        PIPanel.add(DesenhaGraficoResultante());
		contentPane.add(PIPanel);
		
		JLabel lblPotnciaInstantnea = new JLabel("Potencia Instantanea");
		lblPotnciaInstantnea.setHorizontalAlignment(SwingConstants.CENTER);
		lblPotnciaInstantnea.setBounds(90, 340, 200, 15);
		contentPane.add(lblPotnciaInstantnea);
		
		JLabel lblEntradas = new JLabel("Entradas");
		lblEntradas.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEntradas.setHorizontalAlignment(SwingConstants.CENTER);
		lblEntradas.setBounds(325, 11, 100, 25);
		contentPane.add(lblEntradas);
		
		JLabel lblSadas = new JLabel("Saidas");
		lblSadas.setHorizontalAlignment(SwingConstants.CENTER);
		lblSadas.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSadas.setBounds(325, 305, 100, 25);
		contentPane.add(lblSadas);
		//--------------------------------------------------------------------------
		JLabel lblPotenciaLiquida = new JLabel("Potencia Liquida:");
		lblPotenciaLiquida.setBounds(420, 360, 150, 20);
		contentPane.add(lblPotenciaLiquida);
		
		potenciaLiquidaResult = new JFormattedTextField(df.format(potenciaLiquida) + " Watts");
		potenciaLiquidaResult.setHorizontalAlignment(SwingConstants.LEADING);
		potenciaLiquidaResult.setBounds(420, 390, 200, 20);
		potenciaLiquidaResult.setEditable(false);
		contentPane.add(potenciaLiquidaResult);
		//--------------------------------------------------------------------------
		JLabel lblPotenciaDistorcao = new JLabel("Potencia de Distorcao:");
		lblPotenciaDistorcao.setBounds(420, 420, 150, 20);
		contentPane.add(lblPotenciaDistorcao);
		
		float potenciaDistorcao = ( (float) Math.sin( Math.toRadians(tensaoAF - correnteAF) ) * tensaoAmplitude * correnteAmplitude);
		potenciaDistorcaoResult = new JFormattedTextField(df.format(potenciaDistorcao) + " Volt-Ampere");
		potenciaDistorcaoResult.setHorizontalAlignment(SwingConstants.LEADING);
		potenciaDistorcaoResult.setBounds(420, 450, 200, 20);
		potenciaDistorcaoResult.setEditable(false);
		contentPane.add(potenciaDistorcaoResult);
		//--------------------------------------------------------------------------
		JLabel lblTPF = new JLabel("TPF:");
		lblTPF.setBounds(420, 480, 150, 20);
		contentPane.add(lblTPF);
		double F = (double) ( correnteAmplitude * tensaoAmplitude * Math.sin(Math.toRadians((double)(tensaoAF + correnteAF))));
		double P = (double) ( correnteAmplitude * tensaoAmplitude * Math.cos(Math.toRadians((double)(tensaoAF + correnteAF))));
		double TPF = P / F;
		TPFResult = new JFormattedTextField(df.format(TPF) + " VA");
		TPFResult.setHorizontalAlignment(SwingConstants.LEADING);
		TPFResult.setBounds(420, 510, 200, 20);
		TPFResult.setEditable(false);
		contentPane.add(TPFResult);
		//--------------------------------------------------------------------------
		JButton OKButton = new JButton("OK");
        OKButton.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		tensaoPanel.removeAll();
        		tensaoPanel.add(DesenhaGraficoTensao());
        		tensaoPanel.revalidate();
        		tensaoPanel.repaint();
        		
        		correntePanel.removeAll();
        		correntePanel.add(DesenhaGraficoCorrente());
        		correntePanel.revalidate();
        		correntePanel.repaint();
        		
        		PIPanel.removeAll();
        		PIPanel.add(DesenhaGraficoResultante());
        		PIPanel.revalidate();
        		PIPanel.repaint();
        		
        		for (int i = minValueX; i < maxValueX; i = i + step) {
                	double anguloTensaoResult = (double) tensaoAF + (double) (2*Math.PI*60* i);
                	double anguloCorrenteResult = (double)(correnteAF + (double) (ordemHarmonica*2*Math.PI*60*i));
                    potenciaLiquida += (Math.cos(Math.toRadians(((double)(Math.cos(Math.toRadians(anguloTensaoResult)) * tensaoAmplitude)) * ((double)(Math.cos(Math.toRadians(anguloCorrenteResult))) * correnteAmplitude))));
                }
        		potenciaLiquidaResult.setText(df.format(potenciaLiquida) + " Watts");
        		
        		float potenciaDistorcao = ( (float) Math.sin( Math.toRadians(tensaoAF - correnteAF) ) * tensaoAmplitude * correnteAmplitude);
        		potenciaDistorcaoResult = new JFormattedTextField(df.format(potenciaDistorcao) + " Volt-Ampere");
        		
        		double F = (double) ( correnteAmplitude * tensaoAmplitude * Math.sin(Math.toRadians((double)(tensaoAF + correnteAF))));
        		double P = (double) ( correnteAmplitude * tensaoAmplitude * Math.cos(Math.toRadians((double)(tensaoAF + correnteAF))));
        		double TPF = P / F;
        		TPFResult.setText(df.format(TPF) + " VA");
        		
        	}
        });
        OKButton.setBackground(Color.WHITE);
        OKButton.setBounds(650, 260, 60, 20);
        OKButton.setActionCommand("Tensao");
		contentPane.add(OKButton);
		
		//-----------------------------------------------------------------------------
		telaInicial.getContentPane().add(contentPane);
		contentPane.setVisible(true);
		telaInicial.setVisible(true);
	}
	
	private GraphPanel DesenhaGraficoTensao(){
		List<Double> scoresTensao = new ArrayList<>();
		
        for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloTensao = (double) (tensaoAF + (double) (2*Math.PI*60* i));
            scoresTensao.add((double)(Math.cos(Math.toRadians(anguloTensao)) *  tensaoAmplitude));
        }
        GraphPanel grafico = new GraphPanel(scoresTensao);
		
		return grafico;
	}
	
	private GraphPanel DesenhaGraficoCorrente() {
		List<Double >scoresCorrente = new ArrayList<>();
		
        for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloCorrente = (double)(correnteAF + (double) (ordemHarmonica*2*Math.PI*60*i));
            scoresCorrente.add((double)((double)(Math.cos(Math.toRadians(anguloCorrente)) * correnteAmplitude)) );
        }
        
        GraphPanel grafico = new GraphPanel(scoresCorrente);
        
        return grafico;
	}
	
	private GraphPanel DesenhaGraficoResultante() {
		List<Double> scoresPI = new ArrayList<>();
		
        for (int i = minValueX; i < maxValueX; i = i + step) {
        	double anguloTensaoResult = (double) tensaoAF + (double) (2*Math.PI*60* i);
        	double anguloCorrenteResult = (double)(correnteAF + (double) (ordemHarmonica*2*Math.PI*60*i));
            scoresPI.add((Math.cos(Math.toRadians(((double)(Math.cos(Math.toRadians(anguloTensaoResult)) * tensaoAmplitude)) * (double)((Math.cos(Math.toRadians(anguloCorrenteResult))) * correnteAmplitude)))));
        }
        
        GraphPanel grafico = new GraphPanel(scoresPI);
        
        return grafico;
	}
	
}
