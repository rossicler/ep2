package view;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controllers.AcaoDHButton;
import controllers.AcaoFPFButton;
import controllers.AcaoFPHButton;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.Color;
import java.awt.Dimension;

public class TelaPrincipal extends JFrame {

	private JPanel contentPane;
	private JPanel FPFPanel;
	private JPanel DHPanel;
	private JPanel FPHPanel;
	private JPanel SFHPanel;
	private JPanel SVFPanel;
	private JPanel EFPanel;
	private JPanel EATPanel;
	private JPanel FIDPanel;
	private JPanel FTPanel;
	private JPanel RFPanel;
	private JPanel FPanel;
	private JPanel CATPanel;

	/**
	 * Create the frame.
	 */
	public TelaPrincipal(JFrame telaInicial) {
		setBounds(100, 100, 750, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel TituloLabel = new JLabel("Aprenda QEE");
		TituloLabel.setBounds(275, 15, 200, 40);
		TituloLabel.setFont(new Font("Dialog", Font.BOLD, 22));
		TituloLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(TituloLabel);
		
		JPanel funcionalidadesPanel = new JPanel();
		funcionalidadesPanel.setBounds(10, 80, 715, 450);
		funcionalidadesPanel.setPreferredSize(new Dimension(715,2080));
		funcionalidadesPanel.setLayout(null);
		
		JLabel lblSimulaes = new JLabel("Simulacoes");
		funcionalidadesPanel.add(lblSimulaes);
		lblSimulaes.setHorizontalAlignment(SwingConstants.CENTER);
		lblSimulaes.setFont(new Font("Dialog", Font.BOLD, 18));
		lblSimulaes.setBounds(285, 11, 150, 25);
		
		//---------------------------------------------------------------------------------
		JButton FPFButton = new JButton("");
		FPFButton.setBackground(Color.LIGHT_GRAY);
		FPFButton.setBounds(10, 50, 675, 150);
		funcionalidadesPanel.add(FPFButton);
		FPFButton.setLayout(null);
		FPFButton.addActionListener(new AcaoFPFButton(contentPane, telaInicial));
		
		JLabel FPFLabel = new JLabel("Fluxo de potencia fundamental");
		FPFButton.add(FPFLabel);
		FPFLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		FPFLabel.setBounds(10, 110, 220, 25);
		
		FPFPanel = new JPanel();
		FPFButton.add(FPFPanel);
		FPFPanel.setBounds(10, 10, 220, 95);
		FPFPanel.setLayout(null);
		
		//----------------------------------------------------------------------------------
		JButton DHButton = new JButton("");
		DHButton.setBackground(Color.LIGHT_GRAY);
		DHButton.setBounds(10, 220, 675, 150);
		funcionalidadesPanel.add(DHButton);
		DHButton.setLayout(null);
		DHButton.addActionListener(new AcaoDHButton(contentPane, telaInicial));
		
		JLabel DHLabel = new JLabel("Distorcao harmonica");
		DHButton.add(DHLabel);
		DHLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		DHLabel.setBounds(10, 110, 220, 25);
		
		DHPanel = new JPanel();
		DHButton.add(DHPanel);
		DHPanel.setBounds(10, 10, 220, 95);
		DHPanel.setLayout(null);
		
		//----------------------------------------------------------------------------------
		JButton FPHButton = new JButton("");
		FPHButton.setBackground(Color.LIGHT_GRAY);
		FPHButton.setBounds(10, 390, 675, 150);
		funcionalidadesPanel.add(FPHButton);
		FPHButton.setLayout(null);
		FPHButton.addActionListener(new AcaoFPHButton(contentPane, telaInicial));
		
		JLabel FPHLabel = new JLabel("Fluxo de potoncia harmonico");
		FPHButton.add(FPHLabel);
		FPHLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		FPHLabel.setBounds(10, 110, 220, 25);
		
		FPHPanel = new JPanel();
		FPHButton.add(FPHPanel);
		FPHPanel.setBounds(10, 10, 220, 95);
		FPHPanel.setLayout(null);
		
		//----------------------------------------------------------------------------------
		JButton SFHButton = new JButton("");
		SFHButton.setBackground(Color.LIGHT_GRAY);
		SFHButton.setBounds(10, 560, 675, 150);
		funcionalidadesPanel.add(SFHButton);
		SFHButton.setLayout(null);
		
		JLabel SFHLabel = new JLabel("Sequencia de fases dos harmonicos");
		SFHButton.add(SFHLabel);
		SFHLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		SFHLabel.setBounds(10, 110, 220, 25);
		
		SFHPanel = new JPanel();
		SFHButton.add(SFHPanel);
		SFHPanel.setBounds(10, 10, 220, 95);
		SFHPanel.setLayout(null);
		
		//----------------------------------------------------------------------------------
		JButton SVFButton = new JButton("");
		SVFButton.setBackground(Color.LIGHT_GRAY);
		SVFButton.setBounds(10, 730, 675, 150);
		funcionalidadesPanel.add(SVFButton);
		SVFButton.setLayout(null);
		
		JLabel SVFLabel = new JLabel("Sequencia de vetores fundamentais");
		SVFButton.add(SVFLabel);
		SVFLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		SVFLabel.setBounds(10, 110, 220, 25);
		
		SVFPanel = new JPanel();
		SVFButton.add(SVFPanel);
		SVFPanel.setBounds(10, 10, 220, 95);
		SVFPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JButton EFButton = new JButton("");
		EFButton.setBackground(Color.LIGHT_GRAY);
		EFButton.setBounds(10, 900, 675, 150);
		funcionalidadesPanel.add(EFButton);
		EFButton.setLayout(null);
		
		JLabel EFLabel = new JLabel("Efeito flicker");
		EFButton.add(EFLabel);
		EFLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		EFLabel.setBounds(10, 110, 220, 25);
		
		EFPanel = new JPanel();
		EFButton.add(EFPanel);
		EFPanel.setBounds(10, 10, 220, 95);
		EFPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JButton EATButton = new JButton("");
		EATButton.setBackground(Color.LIGHT_GRAY);
		EATButton.setBounds(10, 1070, 675, 150);
		funcionalidadesPanel.add(EATButton);
		EATButton.setLayout(null);
		
		JLabel EATLabel = new JLabel("Efeitos de afundamento da tensao");
		EATButton.add(EATLabel);
		EATLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		EATLabel.setBounds(10, 110, 220, 25);
		
		EATPanel = new JPanel();
		EATButton.add(EATPanel);
		EATPanel.setBounds(10, 10, 220, 95);
		EATPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JButton FIDButton = new JButton("");
		FIDButton.setBackground(Color.LIGHT_GRAY);
		FIDButton.setBounds(10, 1240, 675, 150);
		funcionalidadesPanel.add(FIDButton);
		FIDButton.setLayout(null);
		
		JLabel FIDLabel = new JLabel("Fonte da impedencia de distorcao");
		FIDButton.add(FIDLabel);
		FIDLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		FIDLabel.setBounds(10, 110, 220, 25);
		
		FIDPanel = new JPanel();
		FIDButton.add(FIDPanel);
		FIDPanel.setBounds(10, 10, 220, 95);
		FIDPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JButton FTButton = new JButton("");
		FTButton.setBackground(Color.LIGHT_GRAY);
		FTButton.setBounds(10, 1410, 675, 150);
		funcionalidadesPanel.add(FTButton);
		FTButton.setLayout(null);
		
		JLabel FTLabel = new JLabel("Fator de potencia");
		FTButton.add(FTLabel);
		FTLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		FTLabel.setBounds(10, 110, 220, 25);
		
		FTPanel = new JPanel();
		FTButton.add(FTPanel);
		FTPanel.setBounds(10, 10, 220, 95);
		FTPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JButton RFButton = new JButton("");
		RFButton.setBackground(Color.LIGHT_GRAY);
		RFButton.setBounds(10, 1580, 675, 150);
		funcionalidadesPanel.add(RFButton);
		RFButton.setLayout(null);
		
		JLabel RFLabel = new JLabel("Resposta em frequencia");
		RFButton.add(RFLabel);
		RFLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		RFLabel.setBounds(10, 110, 220, 25);
		
		RFPanel = new JPanel();
		RFButton.add(RFPanel);
		RFPanel.setBounds(10, 10, 220, 95);
		RFPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JButton FButton = new JButton("");
		FButton.setBackground(Color.LIGHT_GRAY);
		FButton.setBounds(10, 1750, 675, 150);
		funcionalidadesPanel.add(FButton);
		FButton.setLayout(null);
		
		JLabel FLabel = new JLabel("Filtros");
		FButton.add(FLabel);
		FLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		FLabel.setBounds(10, 110, 220, 25);
		
		FPanel = new JPanel();
		FButton.add(FPanel);
		FPanel.setBounds(10, 10, 220, 95);
		FPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JButton CATButton = new JButton("");
		CATButton.setBackground(Color.LIGHT_GRAY);
		CATButton.setBounds(10, 1920, 675, 150);
		funcionalidadesPanel.add(CATButton);
		CATButton.setLayout(null);
		
		JLabel CATLabel = new JLabel("Causas do afundamento de tensao");
		CATButton.add(CATLabel);
		CATLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		CATLabel.setBounds(10, 110, 220, 25);
		
		CATPanel = new JPanel();
		CATButton.add(CATPanel);
		CATPanel.setBounds(10, 10, 220, 95);
		CATPanel.setLayout(null);
		
		//---------------------------------------------------------------------------------
		JScrollPane scrollFuncionalidades = new JScrollPane(funcionalidadesPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollFuncionalidades.setBounds(10, 80, 715, 450);
		contentPane.add(scrollFuncionalidades);
		
		telaInicial.add(contentPane);
		telaInicial.setVisible(true);
	}
}
		